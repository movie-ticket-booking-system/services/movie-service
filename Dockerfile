# syntax=docker/dockerfile:1

################################################################################
# Build stage for resolving dependencies
# Utilizes layer caching for dependencies
FROM eclipse-temurin:21-jdk-jammy as deps

WORKDIR /build

# Introduce ARG for Spring profile with a default of 'prod'
ARG SPRING_PROFILES_ACTIVE=prod
ENV SPRING_PROFILES_ACTIVE=${SPRING_PROFILES_ACTIVE}

# Copying Gradle wrapper and scripts
COPY gradlew .
COPY gradle gradle/
COPY build.gradle build.gradle
COPY settings.gradle settings.gradle

# Pre-download dependencies to cache them
RUN ./gradlew --no-daemon dependencies

################################################################################
# Actual build stage that compiles and packages the application
FROM deps as build

# Introduce ARG for Spring profile with a default of 'prod'
ARG SPRING_PROFILES_ACTIVE=prod
ENV SPRING_PROFILES_ACTIVE=${SPRING_PROFILES_ACTIVE}

WORKDIR /build

# Copy source code
COPY src src/

# Build the application, skipping tests for faster builds
# Pass the Spring profile as a gradle property
RUN ./gradlew --no-daemon build -x test -Dspring.profiles.active=${SPRING_PROFILES_ACTIVE}

# Rename the artifact for easier access in later stages
RUN mv build/libs/*-SNAPSHOT.jar build/libs/app.jar

################################################################################
# Final stage to create the runtime image
# Stage 2: Create the runtime image using Alpine for a smaller size
FROM eclipse-temurin:21-jre-alpine as production

# Set a default value for the Spring profile, allowing override at build time
ARG SPRING_PROFILES_ACTIVE=prod
ENV SPRING_PROFILES_ACTIVE=${SPRING_PROFILES_ACTIVE}

# Create a non-root userEntity for running the application
ARG UID=10001
RUN addgroup -S appgroup && adduser -S appuser -G appgroup -u ${UID}
USER appuser

# Copy the built artifact from the build stage
COPY --from=build /build/build/libs/app.jar app.jar

# Expose the application's port
EXPOSE 8080

# Define the command to run the application
ENTRYPOINT ["java", "-jar", "app.jar"]
