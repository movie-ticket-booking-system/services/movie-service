package com.hrishimantra.movieservice;

import com.hrishimantra.movieservice.document.Movie;
import com.hrishimantra.movieservice.repository.MovieRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MovieServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieServiceApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(MovieRepository movieRepository) {
		return args -> {
			System.out.println("Inserting data into MongoDB");
			Movie movie1 = Movie.builder()
					.title("The Dark Knight")
					.description("Batman fights the Joker")
					.genre("Action")
					.language("English")
					.releaseDate("2008")
					.duration("152")
					.ratings("9")
					.build();
			movieRepository.insert(movie1);
			Movie movie = Movie.builder()
					.title("The Dark Knight")
					.description("Batman fights the Joker")
					.genre("Action")
					.language("English")
					.releaseDate("2008")
					.duration("152")
					.ratings("9")
					.build();
			movieRepository.insert(movie);
		};
	}
}
