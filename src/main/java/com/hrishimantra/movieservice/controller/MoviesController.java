package com.hrishimantra.movieservice.controller;

import com.hrishimantra.movieservice.document.Movie;
import com.hrishimantra.movieservice.dto.MovieRequest;
import com.hrishimantra.movieservice.service.MovieService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/ms/movies")
public class MoviesController {

  private final MovieService movieService;

  public MoviesController(MovieService movieService) {
    this.movieService = movieService;
  }


  @GetMapping("/list")
  public List<Movie> listMovies(@RequestParam(required = false) String search) {
    if (search != null && !search.isBlank()) {
      return movieService.search(search);
    } else {
      return movieService.listMovies();
    }
  }

  @GetMapping("/list/{genre}")
  public List<Movie> listMoviesByGenre(@PathVariable("genre") String genre) {
    return movieService.listMoviesByGenre(genre);
  }

  @PostMapping("/add")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public Movie addMovie(MovieRequest movie) {
    return movieService.addMovie(movie);
  }

  @GetMapping("/{id}")
  public Movie getMovie(@PathVariable("id") String id) {
    return movieService.getMovie(id);
  }

  @DeleteMapping("/{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public boolean deleteMovie(@PathVariable String id) {
    return movieService.deleteMovie(id);
  }

  @PutMapping("/{id}")
  @PreAuthorize("hasRole('ROLE_ADMIN')")
  public Movie updateMovie(@PathVariable String id, MovieRequest movie) {
    return movieService.updateMovie(id, movie);
  }
}
