package com.hrishimantra.movieservice.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "movies")
public class Movie {

  @Id
  private String id;
  private String title;

  private String description;

  private String genre;

  private String language;

  private String releaseDate;

  private String duration;

  private String ratings;
}
