package com.hrishimantra.movieservice.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MovieRequest {

  private String title;

  private String description;

  private String genre;

  private String language;

  private String releaseDate;

  private String duration;

  private String ratings;
}
