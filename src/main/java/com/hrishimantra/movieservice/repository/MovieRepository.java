package com.hrishimantra.movieservice.repository;

import com.hrishimantra.movieservice.document.Movie;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface MovieRepository extends MongoRepository<Movie, String> {
  public List<Movie> findByTitle(String title);
  public List<Movie> findByGenre(String genre);

  public List<Movie> findByLanguage(String language);
  public List<Movie> findByRatings(String ratings);

  @Query("{'$or': [{'title': {$regex: ?0, $options: 'i'}}, {'genre': {$regex: ?0, $options: 'i'}}, {'description': {$regex: ?0, $options: 'i'}}, {'language': {$regex: ?0, $options: 'i'}}]}")
  public List<Movie> findByKeyword(String keyword);
}
