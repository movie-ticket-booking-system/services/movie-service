package com.hrishimantra.movieservice.service;

import com.hrishimantra.movieservice.document.Movie;
import com.hrishimantra.movieservice.dto.MovieRequest;
import com.hrishimantra.movieservice.repository.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.hrishimantra.movieservice.util.SetterUtils.setIfNotNull;
import static com.hrishimantra.movieservice.util.SetterUtils.setIfNotNullOrBlank;

@Service
public class MovieService {

  private final MovieRepository movieRepository;

  public MovieService(MovieRepository movieRepository) {
    this.movieRepository = movieRepository;
  }

  public List<Movie> listMovies() {
    return movieRepository.findAll();
  }

  public List<Movie> search(String keyWard) {
    return movieRepository.findByKeyword(keyWard);
  }
  public List<Movie> listMoviesByGenre(String genre) {
    return movieRepository.findByGenre(genre);
  }
  public Movie addMovie(MovieRequest movieRequest) {
    return movieRepository.save(toDocument(movieRequest));
  }

  public Movie getMovie(String id) {
    return movieRepository.findById(id).orElse(null);
  }

  public boolean deleteMovie(String id) {
    Movie movie = movieRepository.findById(id).orElse(null);
    if (movie != null) {
      movieRepository.delete(movie);
      return true;
    }
    return false;
  }

  public Movie updateMovie(String id, MovieRequest movieRequest) {
    Movie movie = movieRepository.findById(id).orElse(null);
    if (movie != null) {
      setIfNotNullOrBlank(movie::setTitle, movieRequest.getTitle());
      setIfNotNullOrBlank(movie::setDescription, movieRequest.getDescription());
      setIfNotNullOrBlank(movie::setGenre, movieRequest.getGenre());
      setIfNotNullOrBlank(movie::setLanguage, movieRequest.getLanguage());
      setIfNotNull(movie::setReleaseDate, movieRequest.getReleaseDate());
      setIfNotNull(movie::setDuration, movieRequest.getDuration());
      setIfNotNull(movie::setRatings, movieRequest.getRatings());
      return movieRepository.save(movie);
    }
    return null;
  }

  public Movie toDocument(MovieRequest movieRequest) {
    return Movie.builder()
            .title(movieRequest.getTitle())
            .description(movieRequest.getDescription())
            .genre(movieRequest.getGenre())
            .language(movieRequest.getLanguage())
            .releaseDate(movieRequest.getReleaseDate())
            .duration(movieRequest.getDuration())
            .ratings(movieRequest.getRatings())
            .build();
  }
}
