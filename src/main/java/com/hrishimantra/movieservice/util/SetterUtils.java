package com.hrishimantra.movieservice.util;

import java.util.function.Consumer;

public class SetterUtils {
  // Handles both null and blank checks for Strings
  public static void setIfNotNullOrBlank(Consumer<String> setter, String value) {
    if (value != null && !value.isBlank()) {
      setter.accept(value);
    }
  }

  // Generic method for non-String attributes
  public static <T> void setIfNotNull(Consumer<T> setter, T value) {
    if (value != null) {
      setter.accept(value);
    }
  }
}
